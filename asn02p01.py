import argparse

def calculate(arr):
    d = {}
    arr = [float(i) for i in arr]
    print(arr)
    for i1 in arr:
        for j1 in arr:
            sub = arr[arr.index(i1):arr.index(j1)+1]
            d[str(sub)] = sum(sub)
    
    maxseq = max(d, key=d.get)
    # print(d)
    maxseq = maxseq.strip('][').split(', ')
    maxseq = [float(i) for i in maxseq]
    print(str(maxseq) + " = " + str(sum(maxseq)))



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-f", "--filename", help="Input File Name")
    args = parser.parse_args()
    filename = args.filename
    

    if(filename):
        f = open(filename, "r")
        sequence = f.readline().split(",")
        calculate(sequence)

    else:
        print("\nError : Invalid Number \nUsage : python " + __file__ + " [options]")
        print("Options : ")
        print("     --filename  : Input File Name")
        print("Example : ")
        print("     python " + __file__ + " --filename input.txt")

if __name__ == "__main__":
    main()